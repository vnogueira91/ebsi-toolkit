const path = require('path');
const ts = require('gulp-typescript');
const gulp = require('gulp');
const {src, dest, parallel, series} = gulp;
const gulpIf = require('gulp-if');
const sourcemaps = require('gulp-sourcemaps');
const merge = require('merge-stream');
const uglify = require('gulp-uglify');
const named = require('vinyl-named');
const webpack = require('webpack-stream');
const rename = require('gulp-rename')
const packageJson = require('./package.json');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;


//Change this to the name of the project
const groupName: string = "@NO-NAME/"

let packageName: string = packageJson.name

const externalsRegistry: string[] = []

if(packageName.indexOf(groupName) !== -1)
    packageName = packageName.substring(groupName.length);

packageName = packageName.replaceAll("-", "_");

const STAGES = {
    BUILD: "build",
    DEPLOY: "deploy"
}

const MODES = {
    DEVELOPMENT: "development",
    PRODUCTION: "production"
}

const defaultOptions : {[indexer: string]: any} = {
    mode: MODES.DEVELOPMENT,
    stage: STAGES.BUILD,
    name: packageName
}

const argParsingRegexp: string = "--([\\w\\-_:\\.;\\d]+)=(([\"']).*?\\3|[^\\s]+)"

const argParser = function(defaultOpts: {[indexer: string]: any}, args: {[indexer: string]: any}, regexp: string | RegExp){
    if (!regexp)
        regexp = argParsingRegexp;
    let cfg = JSON.parse(JSON.stringify(defaultOpts));
    if (!args || args.length < 2)
        return cfg;
    
    const cmd = args.slice(2).join(" ");
    
    regexp = new RegExp(regexp, "g");

    let match;

    const recognized = Object.keys(cfg);

    while(match = regexp.exec(cmd)){
        if (recognized.indexOf(match[1]) === -1)
            continue;

        try {
            cfg[match[1]] = eval(match[2]);
        } catch (e) {
            cfg[match[1]] = match[2];
        }

        if (typeof cfg[match[1]] === 'function')
            throw new Error(`The ${match[1]} argument seems to have been mapped to a function. maybe wrap it in quotes?`)
    }

    return cfg;
}

const config = argParser(defaultOptions, process.argv, argParsingRegexp);

function isDev(){
    return config.mode === MODES.DEVELOPMENT;
}

function isProd(){
    return config.mode === MODES.PRODUCTION;
}

function isBuild(){
    return config.stage === STAGES.BUILD;
}

function isDeploy(){
    return config.stage === STAGES.DEPLOY;
}

function exportLib(){
    const tsProject = ts.createProject('tsconfig.json', {
        target: "es5",
        module: "commonjs",
        declaration: true,
        declarationMap: true,
        emitDeclarationOnly: false,
        isolatedModules: false
    });

    const stream =  src('./src/**/*.ts')
        .pipe(gulpIf(isDev(), sourcemaps.init()))
        .pipe(tsProject())

    return merge([
        stream.dts.pipe(dest("lib")),
        stream.js.pipe(gulpIf(isProd(), uglify())).pipe(gulpIf(isDev(), sourcemaps.write())).pipe(dest("lib"))
    ])
}

function getWebpackConfig(isESM: boolean, useBundleAnalyser: boolean = false){
    const webPackConfig =  {
        mode: config.mode, // can be changed to production to produce minified bundle

        externals:[
            {
            },
            function ({ context, request } : {context: unknown, request: string}, callback: (err?: unknown, result?: string) => void) {
                let regex: string = `^.*?${groupName}\/([^\/]+).*$`
                let regexp = new RegExp(regex, "g"); ///^.*?@noname-project\/([^\/]+).*$/g
                const match = regexp.exec(request)
                if (match) {
                    let name = match[1];
                    externalsRegistry.push(request);
                    return callback(null, `./${name}${isESM ? ".esm" : ""}.js`);
                }
                callback();
            },
        ],

        module: {
            rules: [
                {
                    test: /\.ts$/,
                    use: [{
                        loader: 'ts-loader',
                        options: {
                            configFile: 'tsconfig.json'
                        }
                    }],
                    include:[
                        path.resolve(__dirname, "src")
                    ],
                    exclude: /node_modules/,
                }
            ]
        },

        resolve: {
            extensions: ['.ts', '.js'],
            fallback: {
            }
        },

        output: {
            filename: `${config.name}.bundle.${isProd() ? 'min.' : ''}${isESM ? 'esm.' : ''}js`,
            path: path.resolve(__dirname, "dist/"),
            library: {
                type: "module",
            },
        },
    }

    if(isESM)
        (webPackConfig as any).experiments = {outputModule: true}
    else
        webPackConfig.output = Object.assign({}, webPackConfig.output, {globalObject: 'this', library: config.name, libraryTarget: "umd", umdNamedDefine: true,});


    if (isDev())
        (webPackConfig as any).devtool = 'eval-source-map';

    if(useBundleAnalyser)
        (webPackConfig as any).plugins = [
            new BundleAnalyzerPlugin()
        ]

    return webPackConfig;
}

function exportBundles(isEsm: boolean){
    const entryFile = "src/index.ts"
    return src(entryFile)
        .pipe(named())
        .pipe(webpack(getWebpackConfig(isEsm)))
        .pipe(dest(`./dist${isEsm ? '/esm' : ""}`));
}

function analyzeExportedBundle(){
    const entryFile = "src/index.ts"
    return src(entryFile)
        .pipe(named())
        .pipe(webpack(getWebpackConfig(true, true)))
        .pipe(dest(`./dist/test`));
}

function exportESMDist(){
    return exportBundles(true);
}

function exportJSDist(){
    return exportBundles(false);
}

function testPrep(){
    let operations: any[] = [];

    const group: string = groupName

    const createStream = function (isExternal: boolean, isESM: boolean, moduleName: string){
        return src(`./${isExternal ? `node_modules/${group}${moduleName}/` : ""}dist/${isESM ? "esm/" : ""}${moduleName}.bundle${isESM ? ".esm" : ""}.js`)
            .pipe(rename(`${moduleName}${isESM ? ".esm"  : ""}.js`))
            .pipe(dest("./tests/web/bundles/"))
    }

    externalsRegistry.forEach((key: string) => {
        if(key.includes(groupName)){
            let keySplit = key.split("/");
            operations.push(createStream(true, true, keySplit[1]));
            operations.push(createStream(true, false, keySplit[1]));
        }
    })

 
    operations.push(createStream(false, true, config.name.replaceAll("-", "_")));
    operations.push(createStream(false, false, config.name.replaceAll("-", "_")))


    return merge(operations);
}

function controlFlow(){
    if (isDeploy())
        return series(parallel(exportLib, exportESMDist, exportJSDist), parallel(testPrep));
    if (isBuild())
        return series(exportLib);
}
exports.build = controlFlow()
exports.analyzeBundle = analyzeExportedBundle
exports.default = controlFlow()