## EBSI Toolkit

With documentation, release mechanism and gitlab workflows to match

### Installation

In order to use the ebsi-toolkit package, we need to follow a list of steps presented below.

##### Step 1: Run npm install

To install as a dependency do:
```sh
$ npm install @no-name/nameofpackage
```

To install as a dev dependency do:
```sh
$ npm install @no-name/nameofpackage --save-dev
```
instead.