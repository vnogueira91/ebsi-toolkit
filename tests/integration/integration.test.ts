import {helloWorld} from "../../src";

describe(`TS Workspace Templace`, function(){
    it(`Calls Hello World`, function(){
       require("./testAssets/beforeTest");
       expect(helloWorld()).toEqual(`Hello World`)
    });
});