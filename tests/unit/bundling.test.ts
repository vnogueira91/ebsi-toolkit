import {helloWorld as hello} from "../../src"; // at least one import is needed so the file is considered a module by jest

describe("Distribution Tests", () => {
    it ("reads lib", () => {
        try {
            const {helloWorld} = require("../../lib");
            expect(helloWorld).toBeDefined();
        } catch (e) {
            expect(e).toBeUndefined();
        }

    })

    it ("reads JS Bundle", () => {
        try {
            const {helloWorld} = require("../../dist/ebsi_toolkit.bundle.js");
            expect(helloWorld).toBeDefined();
            
        } catch (e) {
            expect(e).toBeUndefined();
        }
    })
})